﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamarinProject
{
    //tai puslapio dalis, kita yra mainPage.xaml
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            //rodys ne hello world, o hello Khun; butinai turi buti po InitializeComp
            //Lb_Hello.Text = "Hello Khun";
        }     
            //private void OnClick(object sender, EventArgs e)
            //{
            ////kai nuspaudziam mygtuka, Hello world pasikeicia i hello Khun
            //    Lb_Hello.Text = "Hello Khun";
            //}

            private void OnValueChanged(object sender, EventArgs e)
            {
                //kai nuspaudziam mygtuka, Hello world pasikeicia i hello Khun
                //Lb_Hello.Text = "Hello Khun";
                   double val=(sender as Slider).Value;
                   Lb_Hello.Text = val.ToString(); //cia bus skaicius, kuris keisis

                  //arba:(jei norim, kad buto tekstas) Lb_Hello.Text = "Hello Khun";
             }

        private void OnButtonClick(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Page2());
        }
    }
}
