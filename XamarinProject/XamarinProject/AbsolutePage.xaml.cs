﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinProject
{
    public class EntreeSortie
    {
        public double Montant { get; set; }
        public string Type { get; set; }
        public bool IsEntree { get; set; }
        public Guid Id { get; set; }
        public Color Color { get
            {
                return IsEntree ? Color.Green : Color.Red;
                //Entree is in green, Sortie is in red 
            }
        }
    }
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AbsolutePage : ContentPage
	{
        private ObservableCollection<EntreeSortie> entreesSorties =
            new ObservableCollection<EntreeSortie> {
                new EntreeSortie {
                    Montant = 1000,
                    Type = "Pour aller à la mer",
                    IsEntree = false },
                new EntreeSortie{
                    Montant = 2000,
                    Type = "Cadeau de mamy",
                    IsEntree = false }
            }; 

        public ObservableCollection<EntreeSortie> EntreesSorties
        {
            get { return entreesSorties; }
            set { entreesSorties = value; }
        }

        public AbsolutePage ()
		{
            //galima suristi viska:
            this.BindingContext = this;
			InitializeComponent ();
		}

        public EntreeSortie Depense { get; set; } = new EntreeSortie
        {
            IsEntree = false
        };
        public EntreeSortie Entree { get; set; } = new EntreeSortie
        {
            IsEntree = true
        };

        private void OnSavedEntree(object sender, EventArgs e)
        {
            EntreesSorties.Add(new EntreeSortie
            {
                IsEntree=Entree.IsEntree,
                Type=Entree.Type,
                Montant=Entree.Montant
            });
        }

        private void OnSavedDepense(object sender, EventArgs e)
        {
            EntreesSorties.Add(Depense);
            Depense = new EntreeSortie();
        }

        private void OnDelete(object sender, EventArgs e)
        {
            //viska istrina: montant, type, color
            //removeAt istrina pozicija
            var guid = (sender as MenuItem).CommandParameter as Guid?;
            var elem = EntreesSorties.Where(x => x.Id == guid).FirstOrDefault();

            EntreesSorties.Remove(elem);
        }
    }
}